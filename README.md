Comandos git de clase:

$ git init [nombre del proyecto]
Cree un nuevo repositorio local.

$ git clone [url del proyecto]
Descarga un proyecto con todo el historial desde el repositorio remoto

$ git status
Muestra el estado de su directorio de trabajo. Las opciones incluyen nuevos,
archivos preparados y modificados.

$ git add [file]
Agregue un archivo al área de ensayo. 

$ git diff [file]
Muestra cambios entre el directorio de trabajo y el área de ensayo

$ git commit
Crea una nueva confirmación a partir de los cambios agregados al área de ensayo.

$ git branch 
Listar todas las ramas locales en el repositorio

$ git checkout branch
Cambiar el directorio de trabajo a la rama especificada

$ git merge
Úne a la rama especificada en la rama actual 

$ git branch -d
Borra una rama 

$ git pull
Obtiene los cambios desde el repo remoto y fusionar la rama actual

$ git push 
Empuje los cambios locales al control remoto

Comandos Linux

cd -> entrar/cambiar a un directorio

cd .. -> Retroceder una ruta

ls -> mostrar directorios y/o archivos

touch -> creaa un archivo vacio

mkdir -> creaa un directorio

rm -> borrar un archivo

mv -> mover un archivo

cp -> copiar un archivo

vim -> abre vim para editar un archivo 

chmod -> otorgar permisos

tar -> comprimir archivos

nano -> editar un archivo usando nano

echo -> escribir texto

cat -> permite crear, visualizar y concatenar archivos




